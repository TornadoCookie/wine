/* WinRT Windows.Globalization implementation
 *
 * Copyright 2021 Rémi Bernon for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

//#include "initguid.h"
#include "private.h"

WINE_DEFAULT_DEBUG_CHANNEL(locale);

struct calendar
{
    ICalendar ICalendar_iface;
    ITimeZoneOnCalendar ITimeZoneOnCalendar_iface;
    LONG ref;
};

struct calendar_factory
{
    IActivationFactory IActivationFactory_iface;
    ICalendarFactory ICalendarFactory_iface; 
    LONG ref;
};

static inline struct calendar_factory *impl_calendar_factory_from_IActivationFactory(IActivationFactory *iface)
{
    return CONTAINING_RECORD(iface, struct calendar_factory, IActivationFactory_iface);
}

static inline struct calendar_factory *impl_from_ICalendarFactory(ICalendarFactory *iface)
{
    return CONTAINING_RECORD(iface, struct calendar_factory, ICalendarFactory_iface);
}

static inline struct calendar *impl_from_ICalendar(ICalendar *iface)
{
    return CONTAINING_RECORD(iface, struct calendar, ICalendar_iface);
}

static inline struct calendar *impl_from_ITimeZoneOnCalendar(ITimeZoneOnCalendar *iface)
{
    return CONTAINING_RECORD(iface, struct calendar, ITimeZoneOnCalendar_iface);
}

static HRESULT STDMETHODCALLTYPE calendar_QueryInterface(
        ICalendar *iface, REFIID iid, void **out)
{
    struct calendar *calendar = impl_from_ICalendar(iface);

    TRACE("iface %p, iid %s, out %p.\n", iface, debugstr_guid(iid), out);

    if (IsEqualGUID(iid, &IID_IUnknown) ||
        IsEqualGUID(iid, &IID_IInspectable) ||
        IsEqualGUID(iid, &IID_IAgileObject) ||
        IsEqualGUID(iid, &IID_ICalendar))
    {
        IUnknown_AddRef(iface);
        *out = iface;
        return S_OK;
    }

    if (IsEqualGUID(iid, &IID_ITimeZoneOnCalendar))
    {
        IUnknown_AddRef(iface);
        *out = &calendar->ITimeZoneOnCalendar_iface;
        return S_OK;
    }

    FIXME("%s not implemented, returning E_NOINTERFACE.\n", debugstr_guid(iid));
    *out = NULL;
    return E_NOINTERFACE;
}

static ULONG STDMETHODCALLTYPE calendar_AddRef(
        ICalendar *iface)
{
    struct calendar *calendar = impl_from_ICalendar(iface);
    ULONG ref = InterlockedIncrement(&calendar->ref);
    TRACE("iface %p, ref %lu.\n", iface, ref);
    return ref;
}

static ULONG STDMETHODCALLTYPE calendar_Release(
        ICalendar *iface)
{
    struct calendar *calendar = impl_from_ICalendar(iface);
    ULONG ref = InterlockedDecrement(&calendar->ref);

    TRACE("iface %p, ref %lu.\n", iface, ref);

    if (!ref)
        free(calendar);

    return ref;
}

static HRESULT STDMETHODCALLTYPE calendar_GetIids(
        ICalendar *iface, ULONG *iid_count, IID **iids)
{
    FIXME("iface %p, iid_count %p, iids %p stub!\n", iface, iid_count, iids);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_GetRuntimeClassName(
        ICalendar *iface, HSTRING *class_name)
{
    FIXME("iface %p, class_name %p stub!\n", iface, class_name);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_GetTrustLevel(
        ICalendar *iface, TrustLevel *trust_level)
{
    FIXME("iface %p, trust_level %p stub!\n", iface, trust_level);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_Clone(ICalendar *iface, ICalendar **value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_SetToMin(ICalendar *iface)
{
    FIXME("iface %p stub!\n", iface);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_SetToMax(ICalendar *iface)
{
    FIXME("iface %p stub!\n", iface);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_Languages(ICalendar *iface, IVectorView_HSTRING **value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_NumeralSystem(ICalendar *iface, HSTRING *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_put_NumeralSystem(ICalendar *iface, HSTRING value)
{
    FIXME("iface %p, value \"%s\" stub!\n", iface, wine_dbgstr_hstring(value));
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_GetCalendarSystem(ICalendar *iface, HSTRING *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_ChangeCalendarSystem(ICalendar *iface, HSTRING value)
{
    FIXME("iface %p, value \"%s\" stub!\n", iface, wine_dbgstr_hstring(value));
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_GetClock(ICalendar *iface, HSTRING *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_ChangeClock(ICalendar *iface, HSTRING value)
{
    FIXME("iface %p, value \"%s\" stub!", iface, wine_dbgstr_hstring(value));
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_GetDateTime(ICalendar *iface, DateTime *result)
{
    FIXME("iface %p, result %p stub!\n", iface, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_SetDateTime(ICalendar *iface, DateTime value)
{
    FIXME("iface %p stub!\n", iface); /* how would i print value? */
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_SetToNow(ICalendar *iface)
{
    FIXME("iface %p stub!\n", iface);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_FirstEra(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_LastEra(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_NumberOfEras(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_Era(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_put_Era(ICalendar *iface, INT32 value)
{
    FIXME("iface %p, value %d stub!", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_AddEras(ICalendar *iface, INT32 eras)
{
    FIXME("iface %p, eras %d stub!\n", iface, eras);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_EraAsFullString(ICalendar *iface, HSTRING *result)
{
    FIXME("iface %p, result %p stub!\n", iface, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_EraAsString(ICalendar *iface, INT32 ideal_length, HSTRING *result)
{
    FIXME("iface %p, ideal_length %d, result %p stub!\n", iface, ideal_length, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_FirstYearInThisEra(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_LastYearInThisEra(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_NumberOfYearsInThisEra(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_Year(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_put_Year(ICalendar *iface, INT32 value)
{
    FIXME("iface %p, value %d stub!", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_AddYears(ICalendar *iface, INT32 years)
{
    FIXME("iface %p, years %d stub!\n", iface, years);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_YearAsString(ICalendar *iface, HSTRING *result)
{
    FIXME("iface %p, result %p stub!\n", iface, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_YearAsTruncatedString(ICalendar *iface, INT32 remaining_digits, HSTRING *result)
{
    FIXME("iface %p, remaining_digits %d, result %p stub!\n", iface, remaining_digits, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_YearAsPaddedString(ICalendar *iface, INT32 min_digits, HSTRING *result)
{
    FIXME("iface %p, min_digits %d, result %p stub!\n", iface, min_digits, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_FirstMonthInThisYear(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_LastMonthInThisYear(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_NumberOfMonthsInThisYear(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_Month(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_put_Month(ICalendar *iface, INT32 value)
{
    FIXME("iface %p, value %d stub!", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_AddMonths(ICalendar *iface, INT32 months)
{
    FIXME("iface %p, months %d stub!\n", iface, months);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_MonthAsFullString(ICalendar *iface, HSTRING *result)
{
    FIXME("iface %p, result %p stub!\n", iface, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_MonthAsString(ICalendar *iface, INT32 ideal_length, HSTRING *result)
{
    FIXME("iface %p, ideal_length %d, result %p stub!\n", iface, ideal_length, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_MonthAsFullSoloString(ICalendar *iface, HSTRING *result)
{
    FIXME("iface %p, result %p stub!\n", iface, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_MonthAsSoloString(ICalendar *iface, INT32 ideal_length, HSTRING *result)
{
    FIXME("iface %p, ideal_length %d, result %p stub!\n", iface, ideal_length, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_MonthAsNumericString(ICalendar *iface, HSTRING *result)
{
    FIXME("iface %p, result %p stub!\n", iface, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_MonthAsPaddedNumericString(ICalendar *iface, INT32 min_digits, HSTRING *result)
{
    FIXME("iface %p, min_digits %d, result %p stub!\n", iface, min_digits, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_AddWeeks(ICalendar *iface, INT32 weeks)
{
    FIXME("iface %p, weeks %d stub!\n", iface, weeks);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_FirstDayInThisMonth(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_LastDayInThisMonth(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_NumberOfDaysInThisMonth(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_Day(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_put_Day(ICalendar *iface, INT32 value)
{
    FIXME("iface %p, value %d stub!", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_AddDays(ICalendar *iface, INT32 days)
{
    FIXME("iface %p, days %d stub!\n", iface, days);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_DayAsString(ICalendar *iface, HSTRING *result)
{
    FIXME("iface %p, result %p stub!\n", iface, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_DayAsPaddedString(ICalendar *iface, INT32 min_digits, HSTRING *result)
{
    FIXME("iface %p, min_digits %d, result %p stub!\n", iface, min_digits, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_DayOfWeek(ICalendar *iface, DayOfWeek *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_DayOfWeekAsFullString(ICalendar *iface, HSTRING *result)
{
    FIXME("iface %p, result %p stub!\n", iface, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_DayOfWeekAsString(ICalendar *iface, INT32 ideal_length, HSTRING *result)
{
    FIXME("iface %p, ideal_length %d, result %p stub!\n", iface, ideal_length, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_DayOfWeekAsFullSoloString(ICalendar *iface, HSTRING *result)
{
    FIXME("iface %p, result %p stub!\n", iface, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_DayOfWeekAsSoloString(ICalendar *iface, INT32 ideal_length, HSTRING *result)
{
    FIXME("iface %p, ideal_length %d, result %p stub!\n", iface, ideal_length, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_FirstPeriodInThisDay(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_LastPeriodInThisDay(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_NumberOfPeriodsInThisDay(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_Period(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_put_Period(ICalendar *iface, INT32 value)
{
    FIXME("iface %p, value %d stub!", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_AddPeriods(ICalendar *iface, INT32 periods)
{
    FIXME("iface %p, periods %d stub!\n", iface, periods);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_PeriodAsFullString(ICalendar *iface, HSTRING *result)
{
    FIXME("iface %p, result %p stub!\n", iface, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_PeriodAsString(ICalendar *iface, INT32 ideal_length, HSTRING *result)
{
    FIXME("iface %p, ideal_length %d, result %p stub!\n", iface, ideal_length, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_FirstHourInThisPeriod(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_LastHourInThisPeriod(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_NumberOfHoursInThisPeriod(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_Hour(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_put_Hour(ICalendar *iface, INT32 value)
{
    FIXME("iface %p, value %d stub!", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_AddHours(ICalendar *iface, INT32 hours)
{
    FIXME("iface %p, hours %d stub!\n", iface, hours);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_HourAsString(ICalendar *iface, HSTRING *result)
{
    FIXME("iface %p, result %p stub!\n", iface, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_HourAsPaddedString(ICalendar *iface, INT32 min_digits, HSTRING *result)
{
    FIXME("iface %p, min_digits %d, result %p stub!\n", iface, min_digits, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_Minute(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_put_Minute(ICalendar *iface, INT32 value)
{
    FIXME("iface %p, value %d stub!", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_AddMinutes(ICalendar *iface, INT32 minutes)
{
    FIXME("iface %p, minutes %d stub!\n", iface, minutes);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_MinuteAsString(ICalendar *iface, HSTRING *result)
{
    FIXME("iface %p, result %p stub!\n", iface, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_MinuteAsPaddedString(ICalendar *iface, INT32 min_digits, HSTRING *result)
{
    FIXME("iface %p, min_digits %d, result %p stub!\n", iface, min_digits, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_Second(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_put_Second(ICalendar *iface, INT32 value)
{
    FIXME("iface %p, value %d stub!", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_AddSeconds(ICalendar *iface, INT32 seconds)
{
    FIXME("iface %p, seconds %d stub!\n", iface, seconds);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_SecondAsString(ICalendar *iface, HSTRING *result)
{
    FIXME("iface %p, result %p stub!\n", iface, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_SecondAsPaddedString(ICalendar *iface, INT32 min_digits, HSTRING *result)
{
    FIXME("iface %p, min_digits %d, result %p stub!\n", iface, min_digits, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_Nanosecond(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_put_Nanosecond(ICalendar *iface, INT32 value)
{
    FIXME("iface %p, value %d stub!", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_AddNanoseconds(ICalendar *iface, INT32 nanoseconds)
{
    FIXME("iface %p, nanoseconds %d stub!\n", iface, nanoseconds);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_NanosecondAsString(ICalendar *iface, HSTRING *result)
{
    FIXME("iface %p, result %p stub!\n", iface, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_NanosecondAsPaddedString(ICalendar *iface, INT32 min_digits, HSTRING *result)
{
    FIXME("iface %p, min_digits %d, result %p stub!\n", iface, min_digits, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_Compare(ICalendar *iface, ICalendar *other, INT32 *result)
{
    FIXME("iface %p, other %p, result %p stub!\n", iface, other, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_CompareDateTime(ICalendar *iface, DateTime other, INT32 *result)
{
    FIXME("iface %p, other, result %p stub!\n", iface, /*other,*/ result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_CopyTo(ICalendar *iface, ICalendar *other)
{
    FIXME("iface %p, other %p stub!\n", iface, other);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_FirstMinuteInThisHour(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_LastMinuteInThisHour(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_NumberOfMinutesInThisHour(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_FirstSecondInThisMinute(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_LastSecondInThisMinute(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_NumberOfSecondsInThisMinute(ICalendar *iface, INT32 *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_ResolvedLanguage(ICalendar *iface, HSTRING *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_get_IsDaylightSavingTime(ICalendar *iface, boolean *value)
{
    FIXME("iface %p, value %p stub!\n", iface, value);
    return E_NOTIMPL;
}

static const struct ICalendarVtbl calendar_vtbl =
{
    calendar_QueryInterface,
    calendar_AddRef,
    calendar_Release,
    /* IInspectable methods */
    calendar_GetIids,
    calendar_GetRuntimeClassName,
    calendar_GetTrustLevel,
    /* ICalendar methods */
    calendar_Clone,
    calendar_SetToMin,
    calendar_SetToMax,
    calendar_get_Languages,
    calendar_get_NumeralSystem,
    calendar_put_NumeralSystem,
    calendar_GetCalendarSystem,
    calendar_ChangeCalendarSystem,
    calendar_GetClock,
    calendar_ChangeClock,
    calendar_GetDateTime,
    calendar_SetDateTime,
    calendar_SetToNow,
    calendar_get_FirstEra,
    calendar_get_LastEra,
    calendar_get_NumberOfEras,
    calendar_get_Era,
    calendar_put_Era,
    calendar_AddEras,
    calendar_EraAsFullString,
    calendar_EraAsString,
    calendar_get_FirstYearInThisEra,
    calendar_get_LastYearInThisEra,
    calendar_get_NumberOfYearsInThisEra,
    calendar_get_Year,
    calendar_put_Year,
    calendar_AddYears,
    calendar_YearAsString,
    calendar_YearAsTruncatedString,
    calendar_YearAsPaddedString,
    calendar_get_FirstMonthInThisYear,
    calendar_get_LastMonthInThisYear,
    calendar_get_NumberOfMonthsInThisYear,
    calendar_get_Month,
    calendar_put_Month,
    calendar_AddMonths,
    calendar_MonthAsFullString,
    calendar_MonthAsString,
    calendar_MonthAsFullSoloString,
    calendar_MonthAsSoloString,
    calendar_MonthAsNumericString,
    calendar_MonthAsPaddedNumericString,
    calendar_AddWeeks,
    calendar_get_FirstDayInThisMonth,
    calendar_get_LastDayInThisMonth,
    calendar_get_NumberOfDaysInThisMonth,
    calendar_get_Day,
    calendar_put_Day,
    calendar_AddDays,
    calendar_DayAsString,
    calendar_DayAsPaddedString,
    calendar_get_DayOfWeek,
    calendar_DayOfWeekAsFullString,
    calendar_DayOfWeekAsString,
    calendar_DayOfWeekAsFullSoloString,
    calendar_DayOfWeekAsSoloString,
    calendar_get_FirstPeriodInThisDay,
    calendar_get_LastPeriodInThisDay,
    calendar_get_NumberOfPeriodsInThisDay,
    calendar_get_Period,
    calendar_put_Period,
    calendar_AddPeriods,
    calendar_PeriodAsFullString,
    calendar_PeriodAsString,
    calendar_get_FirstHourInThisPeriod,
    calendar_get_LastHourInThisPeriod,
    calendar_get_NumberOfHoursInThisPeriod,
    calendar_get_Hour,
    calendar_put_Hour,
    calendar_AddHours,
    calendar_HourAsString,
    calendar_HourAsPaddedString,
    calendar_get_Minute,
    calendar_put_Minute,
    calendar_AddMinutes,
    calendar_MinuteAsString,
    calendar_MinuteAsPaddedString,
    calendar_get_Second,
    calendar_put_Second,
    calendar_AddSeconds,
    calendar_SecondAsString,
    calendar_SecondAsPaddedString,
    calendar_get_Nanosecond,
    calendar_put_Nanosecond,
    calendar_AddNanoseconds,
    calendar_NanosecondAsString,
    calendar_NanosecondAsPaddedString,
    calendar_Compare,
    calendar_CompareDateTime,
    calendar_CopyTo,
    calendar_get_FirstMinuteInThisHour,
    calendar_get_LastMinuteInThisHour,
    calendar_get_NumberOfMinutesInThisHour,
    calendar_get_FirstSecondInThisMinute,
    calendar_get_LastSecondInThisMinute,
    calendar_get_NumberOfSecondsInThisMinute,
    calendar_get_ResolvedLanguage,
    calendar_get_IsDaylightSavingTime
};

static HRESULT STDMETHODCALLTYPE timezone_on_calendar_QueryInterface(
        ITimeZoneOnCalendar *iface, REFIID iid, void **out)
{
    TRACE("iface %p, iid %s, out %p.\n", iface, debugstr_guid(iid), out);

    if (IsEqualGUID(iid, &IID_IUnknown) ||
        IsEqualGUID(iid, &IID_IInspectable) ||
        IsEqualGUID(iid, &IID_IAgileObject) ||
        IsEqualGUID(iid, &IID_ITimeZoneOnCalendar))
    {
        IUnknown_AddRef(iface);
        *out = iface;
        return S_OK;
    }

    FIXME("%s not implemented, returning E_NOINTERFACE.\n", debugstr_guid(iid));
    *out = NULL;
    return E_NOINTERFACE;
}

static ULONG STDMETHODCALLTYPE timezone_on_calendar_AddRef(
        ITimeZoneOnCalendar *iface)
{
    struct calendar *calendar = impl_from_ITimeZoneOnCalendar(iface);
    ULONG ref = InterlockedIncrement(&calendar->ref);
    TRACE("iface %p, ref %lu.\n", iface, ref);
    return ref;
}

static ULONG STDMETHODCALLTYPE timezone_on_calendar_Release(
        ITimeZoneOnCalendar *iface)
{
    struct calendar *calendar = impl_from_ITimeZoneOnCalendar(iface);
    ULONG ref = InterlockedDecrement(&calendar->ref);

    TRACE("iface %p, ref %lu.\n", iface, ref);

    if (!ref)
        free(calendar);

    return ref;
}

static HRESULT STDMETHODCALLTYPE timezone_on_calendar_GetIids(
        ITimeZoneOnCalendar *iface, ULONG *iid_count, IID **iids)
{
    FIXME("iface %p, iid_count %p, iids %p stub!\n", iface, iid_count, iids);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE timezone_on_calendar_GetRuntimeClassName(
        ITimeZoneOnCalendar *iface, HSTRING *class_name)
{
    FIXME("iface %p, class_name %p stub!\n", iface, class_name);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE timezone_on_calendar_GetTrustLevel(
        ITimeZoneOnCalendar *iface, TrustLevel *trust_level)
{
    FIXME("iface %p, trust_level %p stub!\n", iface, trust_level);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE timezone_on_calendar_GetTimeZone(
        ITimeZoneOnCalendar *iface, HSTRING *value)
{
    TIME_ZONE_INFORMATION tzinfo;
    HRESULT hr;
    DWORD res;

    TRACE("iface %p, value %p\n", iface, value);

    res = GetTimeZoneInformation(&tzinfo);
    
    if (res == TIME_ZONE_ID_INVALID)
    {
        return HRESULT_FROM_WIN32(GetLastError()); 
    }

    if (res == TIME_ZONE_ID_STANDARD || res == TIME_ZONE_ID_UNKNOWN)
    {
        return WindowsCreateString(tzinfo.StandardName, wcslen(tzinfo.StandardName), value);
    }
    else if (res == TIME_ZONE_ID_DAYLIGHT)
    {
        return WindowsCreateString(tzinfo.DaylightName, wcslen(tzinfo.DaylightName), value);
    }

    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE timezone_on_calendar_ChangeTimeZone(
        ITimeZoneOnCalendar *iface, HSTRING time_zone_id)
{
    FIXME("iface %p, time_zone_id \"%s\" stub!\n", iface, wine_dbgstr_hstring(time_zone_id));
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE timezone_on_calendar_TimeZoneAsFullString(
        ITimeZoneOnCalendar *iface, HSTRING *result)
{
    FIXME("iface %p, result %p stub!\n", iface, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE timezone_on_calendar_TimeZoneAsString(
        ITimeZoneOnCalendar *iface, INT32 ideal_length, HSTRING *result)
{
    FIXME("iface %p, ideal_length %d, result %p stub!\n", iface, ideal_length, result);
    return E_NOTIMPL;
}

static const struct ITimeZoneOnCalendarVtbl timezone_on_calendar_vtbl = {
    timezone_on_calendar_QueryInterface,
    timezone_on_calendar_AddRef,
    timezone_on_calendar_Release,
    /* IInspectable methods */
    timezone_on_calendar_GetIids,
    timezone_on_calendar_GetRuntimeClassName,
    timezone_on_calendar_GetTrustLevel,
    /* ITimeZoneOnCalendar methods */
    timezone_on_calendar_GetTimeZone,
    timezone_on_calendar_ChangeTimeZone,
    timezone_on_calendar_TimeZoneAsFullString,
    timezone_on_calendar_TimeZoneAsString
};

static HRESULT STDMETHODCALLTYPE activation_factory_QueryInterface(
        IActivationFactory *iface, REFIID iid, void **out)
{
    struct calendar_factory *factory = impl_calendar_factory_from_IActivationFactory(iface);

    TRACE("iface %p, iid %s, out %p.\n", iface, debugstr_guid(iid), out);

    if (IsEqualGUID(iid, &IID_IUnknown) ||
        IsEqualGUID(iid, &IID_IInspectable) ||
        IsEqualGUID(iid, &IID_IAgileObject) ||
        IsEqualGUID(iid, &IID_IActivationFactory))
    {
        IUnknown_AddRef(iface);
        *out = iface;
        return S_OK;
    }

    if (IsEqualGUID(iid, &IID_ICalendarFactory))
    {
        IUnknown_AddRef(iface);
        *out = &factory->ICalendarFactory_iface;
        return S_OK;
    }

    FIXME("%s not implemented, returning E_NOINTERFACE.\n", debugstr_guid(iid));
    *out = NULL;
    return E_NOINTERFACE;
}

static ULONG STDMETHODCALLTYPE activation_factory_AddRef(IActivationFactory *iface)
{
    struct calendar_factory *factory = impl_calendar_factory_from_IActivationFactory(iface);
    return InterlockedIncrement(&factory->ref);
}

static ULONG STDMETHODCALLTYPE activation_factory_Release(IActivationFactory *iface)
{
    struct calendar_factory *factory = impl_calendar_factory_from_IActivationFactory(iface);
    return InterlockedDecrement(&factory->ref);
}

static HRESULT WINAPI activation_factory_GetIids( IActivationFactory *iface, ULONG *iid_count, IID **iids )
{
    FIXME( "iface %p, iid_count %p, iids %p stub!\n", iface, iid_count, iids );
    return E_NOTIMPL;
}

static HRESULT WINAPI activation_factory_GetRuntimeClassName( IActivationFactory *iface, HSTRING *class_name )
{
    FIXME( "iface %p, class_name %p stub!\n", iface, class_name );
    return E_NOTIMPL;
}

static HRESULT WINAPI activation_factory_GetTrustLevel( IActivationFactory *iface, TrustLevel *trust_level )
{
    FIXME( "iface %p, trust_level %p stub!\n", iface, trust_level );
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE activation_factory_ActivateInstance(
        IActivationFactory *iface, IInspectable **out)
{
    struct calendar *calendar;

    TRACE("iface %p, out %p.\n", iface, out);

    if (!(calendar = calloc(1, sizeof(*calendar)))) return E_OUTOFMEMORY;
    calendar->ICalendar_iface.lpVtbl = &calendar_vtbl;
    calendar->ITimeZoneOnCalendar_iface.lpVtbl = &timezone_on_calendar_vtbl;
    calendar->ref = 1;

    *out = (IInspectable *)&calendar->ICalendar_iface;
    return S_OK;
}

static const struct IActivationFactoryVtbl activation_factory_vtbl =
{
    activation_factory_QueryInterface,
    activation_factory_AddRef,
    activation_factory_Release,
    /* IInspectable methods */
    activation_factory_GetIids,
    activation_factory_GetRuntimeClassName,
    activation_factory_GetTrustLevel,
    /* IActivationFactory methods */
    activation_factory_ActivateInstance,
};

static HRESULT STDMETHODCALLTYPE calendar_factory_QueryInterface(
        ICalendarFactory *iface, REFIID iid, void **object)
{
    struct calendar_factory *factory = impl_from_ICalendarFactory(iface);
    return IActivationFactory_QueryInterface(&factory->IActivationFactory_iface, iid, object);
}

static ULONG STDMETHODCALLTYPE calendar_factory_AddRef(
        ICalendarFactory *iface)
{
    struct calendar_factory *factory = impl_from_ICalendarFactory(iface);
    return IActivationFactory_AddRef(&factory->IActivationFactory_iface);
}

static ULONG STDMETHODCALLTYPE calendar_factory_Release(
        ICalendarFactory *iface)
{
    struct calendar_factory *factory = impl_from_ICalendarFactory(iface);
    return IActivationFactory_Release(&factory->IActivationFactory_iface);
}

static HRESULT STDMETHODCALLTYPE calendar_factory_GetIids(
        ICalendarFactory *iface, ULONG *iid_count, IID **iids)
{
    FIXME("iface %p, iid_count %p, iids %p stub!\n", iface, iid_count, iids);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_factory_GetRuntimeClassName(
        ICalendarFactory *iface, HSTRING *class_name)
{
    FIXME("iface %p, class_name %p stub!\n", iface, class_name);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_factory_GetTrustLevel(
        ICalendarFactory *iface, TrustLevel *trust_level)
{
    FIXME("iface %p, trust_level %p stub!\n", iface, trust_level);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_factory_CreateCalendarDefaultCalendarAndClock(
        ICalendarFactory *iface, IIterable_HSTRING *languages, ICalendar **result)
{
    FIXME("iface %p, languages %p, result %p stub!\n", iface, languages, result);
    return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE calendar_factory_CreateCalendar(
        ICalendarFactory *iface, IIterable_HSTRING *languages, HSTRING calendar, HSTRING clock,
        ICalendar **result)
{
    FIXME("iface %p, languages %p, calendar \"%s\", clock \"%s\", result %p stub!", iface,
            languages, wine_dbgstr_hstring(calendar), wine_dbgstr_hstring(clock), result);
    return E_NOTIMPL;
}

static const struct ICalendarFactoryVtbl calendar_factory_vtbl =
{
    calendar_factory_QueryInterface,
    calendar_factory_AddRef,
    calendar_factory_Release,
    /* IInspectable methods */
    calendar_factory_GetIids,
    calendar_factory_GetRuntimeClassName,
    calendar_factory_GetTrustLevel,
    /* ICalendarFactory methods */
    calendar_factory_CreateCalendarDefaultCalendarAndClock,
    calendar_factory_CreateCalendar
};

static struct calendar_factory factory =
{
    {&activation_factory_vtbl},
    {&calendar_factory_vtbl},
    0
};

IActivationFactory *calendar_factory = &factory.IActivationFactory_iface;

